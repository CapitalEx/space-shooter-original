
button = {}
menu = {}
menu.font = love.graphics.newFont(60)

menu.color = 255

square = {}
square.t = 0

hover = {}

function menu.draw()
	button.draw()
end

function button.create(fx,fy,ftext,fid)
	local b = {}
	b.x = fx
	b.y = fy
	b.text = ftext
	b.id = fid
	table.insert(button, b)
end

function menu.load()
	
	button.create(love.graphics.getWidth()/5, love.graphics.getHeight()*(1/3) - menu.font:getHeight('play')/2,'play','play')
	button.create(love.graphics.getWidth()/5, love.graphics.getHeight()*(2/3) - menu.font:getHeight('play')/2,'exit','exit')

end

function button.draw()
	love.graphics.setFont(menu.font)
	love.graphics.setColor(255,255,255)
	for i,v in ipairs(button) do
		love.graphics.print(v.text,v.x,v.y)
	end
	love.graphics.print('SPACE SHOOTER', 100, menu.font:getHeight('SPACE SHOOTER')/3,0,1,1,0,0,-0.5,0)

end

function button.click(mx,my)
	for i, v in ipairs(button) do
		if mx < v.x + menu.font:getWidth(v.text) and mx > v.x
		and my > v.y and my < v.y + menu.font:getHeight(v.text) then
			if v.id == 'play' then
				gamestate = 'play'
			elseif v.id == 'exit' then
				love.event.quit()
			else
				gamestate = 'menu'
			end
		end	
	end
end
function button.background()
	for i,v in ipairs(square) do
		love.graphics.setColor(v.c,v.c,v.c)
		love.graphics.rectangle('fill',v.x,v.y, v.size, v.size)
	end
end

function menu.update(dt)
	math.randomseed(os.time()*math.random())
	m = {}
	m.x, m.y = love.mouse.getPosition()
	local remSquare = {}
	square.t = square.t + dt
	if square.t > 2  or #square < 100 then
		square.spawn = true
		square.spawn = 0
	else
		square.spawn = false
	end
	if square.spawn then
		local s = {}
		s.x = love.graphics.getWidth()
		s.y = math.random(140, 440)
		s.c = math.random(55,155)
		s.sp = math.random(50,250)
		s.size = math.random(8, 64) 
		table.insert(square, s)
	end
	for i,v in ipairs(square) do
		v.x = v.x - v.sp * dt
		if v.x < 0 then
			table.insert(remSquare, i)
		end
	end
	for i,v in ipairs(remSquare) do
		table.remove(square, v)
	end
	return m
end
