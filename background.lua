bg ={}

stars = {}
stars.t = 0

planets = {}
planets.t = 0

clouds = {}


function bg.draw()
	for i,v in ipairs(stars) do
		love.graphics.setColor(255,255,155)
		love.graphics.circle('fill',v.x,v.y,v.r, 10)
	end
	for i,v in ipairs(planets) do
		love.graphics.setColor(v.r,v.g,v.b)
		love.graphics.circle('fill', v.x, v.y, v.radus, 10)
	end
	for i,v in ipairs(clouds) do
		love.graphics.setColor(233,233,233)
		love.graphics.rectangle('fill',v.x,v.y,v.h,v.w)
	end
end

function bg.update(dt)
	math.randomseed(os.time()*math.random())
	local remStar = {}
	local remPlanet = {}
	local remCloud = {}
	for i,v in ipairs(stars) do
		v.y = v.y + v.s * dt
		if v.y > love.graphics.getHeight() then
			table.insert(remStar, i)
		end
	end
	for i,v in ipairs(remStar) do
		table.remove(stars, v)
	end
	if #stars < 125 then
		stars.spawn = true
		stars.t = 0
	else
		stars.spawn = false
	end
	if stars.spawn then
		local d = {}
		d.x = math.random(0, love.graphics.getWidth())
		d.y = 0
		d.r = math.random(0.5,1)
		d.s = math.random(200,300)
		table.insert(stars, d)
	end
	planets.t = planets.t + dt * 1000
	if planets.t > math.random(2000, 3000) then
		planets.spawn = true
		planets.t = 0
	else
		planets.spawn = false
	end
	if planets.spawn then
		local p = {}
		local cl = {}
		p.x = math.random(0, love.graphics.getWidth())
		p.radus = math.random(10, 30)
		p.y = 0 - p.radus
		p.s = math.random(100, 200)
		p.r = math.random(10, 255)
		p.g = math.random(10, 255)
		p.b = math.random(10, 255)
		table.insert(planets, p)
	end
	for i,v in ipairs(planets) do
		v.y = v.y + v.s * dt
		if v.y - v.r > love.graphics.getHeight() then
			table.insert(remPlanet, i)
			table.insert(remCloud, ii)
		end
	end
	
	for i,v in ipairs(remPlanet) do
		table.remove(planets, v)
	end
	for i,v in ipairs(remCloud) do
		table.remove(clouds, v)
	end
end
