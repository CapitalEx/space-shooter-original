ene = {}

ene.r = love.graphics.rectangle
ene.spawn = 0
ene.doSpawn = false
ene.delay = .6 
ene.speed = 100
ene.info = {}
ene.speedMod = 0
ene.timer = 0

function ene.draw()
	for i,v in ipairs(ene.info) do
		love.graphics.setColor(200,0,0)
		ene.r('fill',v.x,v.y,32,32)
	end
	--love.graphics.print(#ene.info,100,100)
	--love.graphics.print(ene.timer..'\n'..ene.speed..'\n'..ene.delay, 100,100)
	
end
	
function ene.update(dt)
	math.randomseed(os.time()*math.random())
	ene.spawn = ene.spawn + dt
	ene.create()
	ene.destroy()
	ene.move(dt)
end

function ene.move(dt)
	ene.timer = ene.timer + dt
	
	if ene.timer > 10 then
		ene.speed = ene.speed + 20
		ene.delay = ene.delay - .05
		ene.timer = 0
		if ene.delay < 0 then
			ene.delay = .05
		end
	end
	
	for i,v in ipairs(ene.info) do
		v.y = v.y + ene.speed * dt
	end
end

function ene.create()
	if ene.spawn > ene.delay then
		ene.doSpawn = true
		ene.spawn = 0
	else
		ene.doSpawn = false
	end
	if ene.doSpawn then
		local a = {}
		a.x = math.random(32, love.graphics.getWidth())
		a.y = 0
		table.insert(ene.info, a)
	else
	
	end
end

function ene.destroy()
	local remEne = {}
	local remShot = {}
	for i,v in ipairs(ene.info) do
		if v.y > love.graphics.getHeight() then
			table.insert(remEne, i)
		end
		for ii,vv in ipairs(ship.shoot) do
			if vv.x > v.x and vv.x < v.x + 32 and 
			vv.y < v.y + 32 and vv.y > v.y then
				table.insert(remEne, i)
				table.insert(remShot, ii)
				ship.score = ship.score + 1
			end
		end
	end
	for i,v in ipairs(remEne) do
		table.remove(ene.info, v)
	end
	for i,v in ipairs(remShot) do
		table.remove(ship.shoot, v)
	end
end
