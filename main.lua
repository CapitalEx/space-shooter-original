require 'ship'
require 'enemies'
require 'menu'
require 'background'
function love.load()
	menu.load()
	gamestate = 'menu'
	gameover_font = love.graphics.newFont(72)
end

function love.draw()
	if gamestate == 'play' or gamestate == 'gameover' then
		bg.draw()
		ship.draw()
		ene.draw()
	end
	if gamestate == 'menu' then
		button.background()
		menu.draw()
	end
	if gamestate == 'gameover' then
		love.graphics.setFont(gameover_font)
		love.graphics.setColor(255,255,255)
		love.graphics.print("GAME OVER\n"..'Score: '..ship.score,love.graphics.getWidth()/2 - gameover_font:getWidth("GAME OVER")/2, love.graphics.getHeight()/2 - gameover_font:getHeight("GAME OVER"))
	end
end

function love.update(dt)
	if gamestate == 'play' then
		ship.update(dt)
		ene.update(dt)
		bg.update(dt)
	end
	if gamestate == 'menu' then
		menu.update(dt)
	end
end

function love.mousepressed()
	if gamestate == 'menu' then
		button.click(love.mouse.getPosition( ))
	end
	if gamestate == 'play' then
		ship.pew()
	end
end

--[[function love.mouserealeased(button)
	if button == 'l' then
		ship.pew()
	end
end]]
