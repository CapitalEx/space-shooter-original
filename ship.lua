ship = {}
ship.window = {love.graphics.getWidth(), love.graphics.getHeight()}
ship.p ={
	{ship.window[1]/2, ship.window[2]-64};
	{(ship.window[1]/2) - 16, ship.window[2]-32};
	{(ship.window[1]/2) + 16, ship.window[2]-32};
}

--[[ship.left
ship.right]]

ship.shoot = {}

ship.hit = false

ship.score = 0
ship.f = love.graphics.newFont(16)

function ship.draw()
	love.graphics.setColor(255,220,0)
	love.graphics.polygon('fill',ship.p[1][1],ship.p[1][2],ship.p[2][1],ship.p[2][2],ship.p[3][1],ship.p[3][2])
	for i, v in ipairs(ship.shoot) do
		love.graphics.setColor(255,255,0)
		love.graphics.rectangle('fill',v.x,v.y,2,4)
	end
	love.graphics.setFont(ship.f)
	love.graphics.print(ship.score, 0 + ship.f:getWidth(ship.score),0 + ship.f:getHeight(ship.score))
end

function ship.update(dt)
	ship.move(dt)
	ship.col()
end

function ship.move(dt)
	local mX = love.mouse.getX()
	local d = {
			mX - ship.p[1][1],
			mX - ship.p[2][1]-16,
			mX - ship.p[3][1]+16
	}


	ship.p[1][1] = ship.p[1][1] + d[1] * dt * 10
	ship.p[2][1] = ship.p[2][1] + d[2] * dt * 10
	ship.p[3][1] = ship.p[3][1] + d[3] * dt * 10
	for i,v in ipairs(ship.shoot) do
		v.x = v.x
		v.y = v.y - 150*dt
	end
end

function ship.col()
	local remShot = {}
	for i,v in ipairs(ene.info) do
		if ship.p[1][1] > v.x and ship.p[1][1] < v.x + 32 and
		ship.p[1][2] > v.y and ship.p[1][2] < v.y + 32 then
			gamestate = 'gameover'
		elseif ship.p[2][1] > v.x and ship.p[2][1] < v.x + 32 and
		ship.p[2][2] > v.y and ship.p[2][2] < v.y + 32 then
			gamestate = 'gameover'
		elseif ship.p[3][1] > v.x and ship.p[3][1] < v.x + 32 and
		ship.p[3][2] > v.y and ship.p[3][2] < v.y + 32 then
			gamestate = 'gameover'
		else
			ship.hit = false
		end		
	end
	for i,v in ipairs(ship.shoot) do
		if v.y < 0 then
			table.insert(remShot, i)
		end
	end
	for i,v in ipairs(remShot) do
		table.remove(ship.shoot, v)
	end		
end
	
function ship.pew()
	local a = {}
	a.x = ship.p[1][1]
	a.y = ship.p[1][2]
	table.insert(ship.shoot, a)
end
